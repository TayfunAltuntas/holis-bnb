import { Controller, Get } from '@nestjs/common';
import { CategoryService } from './Category.service';

@Controller('categories')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  /** List all locations in database with this endpoint */
  @Get()
  async getCategories() {
    return await this.categoryService.getCategories();
  }
}
