import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Location } from '../locations/Location.entity';

/**
 * - You should make sure they are properly decorated for typeorm
 */
@Entity()
export class Category extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'text', name: 'name', unique: true })
  name: string;

  @Column()
  description: string;

  @OneToMany(() => Location, (location) => location.category)
  locations: Location[];
}
