import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { makeid } from 'src/utils/generateId';
import { Repository } from 'typeorm';
import { Category } from '../categories/Category.entity';
import { ExampleLocationDto } from './Location.dto';
import { Location } from './Location.entity';
@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Location)
    private readonly locationRepository: Repository<Location>,
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>
  ) {}

  async getLocations() {
    return await this.locationRepository.find();
  }

  async getOneLocationById(id: number) {
    const location = await this.locationRepository.findOne(id);
    if (location === undefined) {
      throw new NotFoundException('Location not found');
    }
    return location;
  }

  async getOneLocationByIdWithCategory(id: number) {
    const location = await this.locationRepository.findOne(id, {
      relations: ['category'],
    });

    if (location === undefined) {
      throw new NotFoundException('Location not found');
    }
    return location;
  }

  async deleteOneLocationById(id: number) {
    this.locationRepository.delete(id);
  }

  async create(locationData: ExampleLocationDto): Promise<Location> {
    const location = new Location();

    location.title = locationData.title;
    location.description = locationData.description;
    location.categoryId = locationData.categoryId;
    location.picture = locationData.picture;
    location.location = locationData.location;
    location.numberOfRooms = locationData.numberOfRooms;
    location.stars = locationData.stars;
    location.price = locationData.price;

    const category = await this.categoryRepository.findOne(location.categoryId);
    if (!category) {
      const newCategory = new Category();
      newCategory.id = location.categoryId;
      newCategory.name = makeid(10);
      newCategory.description = makeid(10);
      await this.categoryRepository.save(newCategory);
      location.category = newCategory;
    }
    return this.locationRepository.save(location);
  }
}
