import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ExampleLocationDto } from './Location.dto';
import { LocationService } from './Location.service';

@Controller('locations')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  /** List all locations in database with this endpoint */
  @Get()
  async getLocations() {
    return await this.locationService.getLocations();
  }

  @Get(':id')
  async getOneLocationById(@Param() params) {
    return await this.locationService.getOneLocationByIdWithCategory(params.id);
  }

  @Delete(':id')
  async deleteOneLocationById(@Param() params) {
    return await this.locationService.deleteOneLocationById(params.id);
  }

  @Post()
  async create(@Body() createLocationDto: ExampleLocationDto) {
    return await this.locationService.create(createLocationDto);
  }
}
