import React from 'react';
import './Card.css';
import { Grid } from '@mui/material';
import { ILocation } from '../../types/locationType';

interface ICard extends ILocation {
  // eslint-disable-next-line no-unused-vars
  onClickCard?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>, id: number) => void;
}

const Card: React.FC<ICard> = (props) => {
  const { title, location, picture, price, onClickCard } = props;

  return (
    <Grid container item lg={3} className="card">
      <div
        className="card-container"
        onClick={(event) => onClickCard && onClickCard(event, props.id)}>
        <img className="card-img" src={picture} alt={picture} />
        <Grid container flexDirection="column">
          <span className="card-location-span">{location}</span>
          <span className="card-text-span">{title}</span>
          <span className="card-location-span">
            €{price.toLocaleString()}
            <span className="card-text-span">night</span>
          </span>
        </Grid>
      </div>
    </Grid>
  );
};

export default React.memo(Card);
