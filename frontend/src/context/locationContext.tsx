import React, { PropsWithChildren } from 'react';
import { LocationContextType } from '../types/locationType';

export const LocationContext = React.createContext<LocationContextType>({
  inputSearch: '',
  setInputSearch: () => {}
});

export const LocationProvider = (props: PropsWithChildren) => {
  // handling states for Location
  const [inputSearch, setInputSearch] = React.useState('');

  return (
    <LocationContext.Provider value={{ inputSearch, setInputSearch }}>
      {props.children}
    </LocationContext.Provider>
  );
};

export const useLocationContext = () => {
  return React.useContext(LocationContext);
};
