import React from 'react';
import SearchPage from './pages/Search/Search';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Layout from './layouts/Layout';
import { LocationProvider } from './context/locationContext';
import DisplayLocationPage from './pages/DisplayLocation/DisplayLocation';

function App() {
  return (
    <LocationProvider>
      <Router>
        <Routes>
          <Route
            element={
              <Layout
              //handleInputSearch={handleInputSearch}
              />
            }>
            <Route path="/location/:locationId" index element={<DisplayLocationPage />} />
            <Route path="/" index element={<SearchPage />} />
          </Route>
        </Routes>
      </Router>
    </LocationProvider>
  );
}

export default App;
