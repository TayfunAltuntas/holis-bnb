import { Button, Grid, TextField } from '@mui/material';
import axios from 'axios';
import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { ILocation } from '../../types/locationType';
import './DisplayLocation.css';

type DisplayLocationPageProps = {};

const DisplayLocationPage: React.FC<DisplayLocationPageProps> = () => {
  const searchParams = useParams();
  const [isReqFetched, setIsReqFetched] = React.useState(false);
  const [isReqFailed, setIsReqFailed] = React.useState(false);
  const [location, setLocation] = React.useState<ILocation>();
  const navigate = useNavigate();

  const fetchOneLocation = React.useCallback(async () => {
    if (searchParams?.locationId) {
      try {
        const getLocation = await axios.get<ILocation>('/locations/' + searchParams.locationId);
        if (getLocation) {
          setLocation(getLocation.data);
          setIsReqFetched(true);
        }
      } catch (error) {
        setIsReqFailed(true);
        console.log(error);
      }
    }
  }, [searchParams.locationId]);

  React.useEffect(() => {
    fetchOneLocation();
  }, [fetchOneLocation]);
  // Create a function to handle price change and persist it to database

  // Create a function to delete the location and persist it to database
  const deleteLocation = async () => {
    try {
      await axios.delete('/locations/' + searchParams.locationId);
      navigate('/');
    } catch (error) {
      console.log(error);
    }
  };

  const roomDisplayed =
    location && location?.numberOfRooms <= 1
      ? location?.numberOfRooms + ' room'
      : location?.numberOfRooms + ' rooms';

  return isReqFailed ? (
    <h1>Nous n&apos;avons pas trouvé la location</h1>
  ) : !isReqFetched || !location ? (
    <h1>Loading</h1>
  ) : (
    <div className="display-location">
      <div className="display-location__image-container">
        <img className="display-location__image" alt="background-image" src={location.picture} />
      </div>

      <Grid container className="display-location__content">
        <Grid container justifyContent="space-between" alignItems="center" item xs={7}>
          <h1 className="display-location__title">{location.title}</h1>
          <span className="display-location__price">
            €{location.price.toLocaleString()}{' '}
            <span className="display-location__night">night</span>
          </span>
          <Grid container>
            <span>
              {location.category?.name} - {roomDisplayed}
            </span>
          </Grid>
          <Grid container>
            <span>{location.description}</span>
          </Grid>
          <Grid container>
            <span>{location.category?.description}</span>
          </Grid>
        </Grid>
        <Grid container justifyContent="center" alignItems="center" item xs={5}>
          <div
            style={{
              border: '1px solid gray',
              height: '200px',
              width: '50%',
              borderRadius: '25px',
              padding: '1rem',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between'
            }}>
            <Grid container>
              <span style={{ fontWeight: 500, color: '#838383' }}>Modify price</span>
            </Grid>
            <Grid container justifyContent="center">
              <TextField
                defaultValue={location.price}
                label="Price"
                variant="outlined"
                fullWidth
                placeholder="modify price"
              />
            </Grid>
            <Grid container justifyContent="space-evenly">
              <Button
                variant="outlined"
                style={{ borderColor: '#04ddbf', color: '#04ddbf' }}
                onClick={deleteLocation}>
                Delete
              </Button>
              <Button variant="contained" style={{ backgroundColor: '#04ddbf', color: 'white' }}>
                Confirm
              </Button>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default DisplayLocationPage;
