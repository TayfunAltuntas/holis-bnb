import React from 'react';
import './Search.css';
import {
  IIndexedCategoriesLocation,
  IIndexedRoomsLocations,
  ILocation
} from '../../types/locationType';
import axios from 'axios';
import { Grid, Divider } from '@mui/material';
import Card from '../../components/Card/Card';
import { ICategory } from '../../types/categoryType';
import { useLocationContext } from '../../context/locationContext';
import { useNavigate } from 'react-router-dom';

type SearchPageProps = {};

const SearchPage: React.FC<SearchPageProps> = () => {
  const [allLocations, setAllLocations] = React.useState<ILocation[]>();
  const [allCategories, setAllCategories] = React.useState<ICategory[]>();
  const { inputSearch } = useLocationContext();
  const navigate = useNavigate();

  // Create a function to fetch all locations from database
  const fetchAllLocations = React.useCallback(async () => {
    try {
      const getAllLocations = await axios.get<ILocation[]>('/locations');
      if (getAllLocations) {
        setAllLocations(getAllLocations.data);
      }
    } catch (error) {
      console.log(error);
    }
  }, []);

  // Get all Categories from the API
  const fetchAllCategories = React.useCallback(async () => {
    try {
      const getAllLocations = await axios.get<ICategory[]>('/categories');
      if (getAllLocations) {
        setAllCategories(getAllLocations.data);
      }
    } catch (error) {
      console.log(error);
    }
  }, []);

  React.useEffect(() => {
    fetchAllLocations();
    fetchAllCategories();
  }, [fetchAllLocations, fetchAllCategories]);

  // Create a function to sort locations by categories & by number of rooms
  const sortedLocations = React.useMemo(() => {
    const sortedArray = allLocations?.sort(
      (value, b) => value.categoryId - b.categoryId || value.numberOfRooms - b.numberOfRooms
    );

    return sortedArray?.reduce((accumulator: IIndexedCategoriesLocation, curValue) => {
      const { categoryId, numberOfRooms } = curValue;
      const categoryName = allCategories?.find((category) => category.id === categoryId)?.name;
      if (categoryName) {
        accumulator[categoryName] = accumulator[categoryName] ?? {};
        accumulator[categoryName][numberOfRooms] = accumulator[categoryName][numberOfRooms] ?? [];
        accumulator[categoryName][numberOfRooms].push(curValue);
      }
      return accumulator;
    }, {});
  }, [allLocations]);

  // Function to display Cards
  const displayCards = (roomsAndLocations: ILocation[] | undefined) => {
    const navigateToLocationDetail = (
      event: React.MouseEvent<HTMLDivElement, MouseEvent>,
      id: number
    ) => {
      event.preventDefault();
      navigate(`/location/${id}`);
    };
    return roomsAndLocations?.map((element, key) => (
      <Card {...element} onClickCard={navigateToLocationDetail} key={key} />
    ));
  };

  // Function to display the container for the rooms and the cards
  const displayRooms = (sortedLocationsByRooms: IIndexedRoomsLocations) => {
    const roomsAndCards = sortedLocationsByRooms ? Object.entries(sortedLocationsByRooms) : [];

    return roomsAndCards.map((element, key) => {
      const room = parseInt(element[0]) <= 1 ? 'room' : 'rooms';
      return (
        <Grid container key={key}>
          <Grid container item xs={2}>
            <h2>{element[0] + ' ' + room}</h2>
          </Grid>
          {displayCards(element[1])}
        </Grid>
      );
    });
  };

  // Function to display the container for the Categories, Rooms and Cards
  const displayCategories = (
    sortedLocationsBySubCriterias: IIndexedCategoriesLocation | undefined
  ) => {
    const categoriesAndRooms = sortedLocationsBySubCriterias
      ? Object.entries(sortedLocationsBySubCriterias)
      : [];

    return categoriesAndRooms.map((element, key) => (
      <Grid container key={key}>
        <>
          <h1>{element[0]}</h1>
          <Divider style={{ width: '97%' }} />
        </>
        <Grid container>{displayRooms(element[1])}</Grid>
      </Grid>
    ));
  };

  // Function to filter locations by title when typing in the search bar
  const filteredPersons = allLocations?.filter((location) =>
    location.title.toLowerCase().includes(inputSearch.toLowerCase())
  );

  // Function to display the Container and cards when typing in the search bar
  const displayFilteredBySearchCards = () => {
    return <Grid container>{displayCards(filteredPersons)}</Grid>;
  };

  return (
    <div className="search">
      {!inputSearch ? displayCategories(sortedLocations) : displayFilteredBySearchCards()}
    </div>
  );
};

export default SearchPage;
