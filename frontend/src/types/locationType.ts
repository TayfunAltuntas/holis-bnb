import React from 'react';
import { ICategory } from './categoryType';

export interface ILocation {
  id: number;
  title: string;
  description: string;
  location: string;
  picture: string;
  stars: number;
  numberOfRooms: number;
  price: number;
  categoryId: number;
  category?: ICategory;
}

export interface IIndexedRoomsLocations {
  [key: number]: ILocation[];
}

export interface IIndexedCategoriesLocation {
  [key: string]: IIndexedRoomsLocations;
}

export type LocationContextType = {
  inputSearch: string;
  setInputSearch: React.Dispatch<React.SetStateAction<string>>;
};
